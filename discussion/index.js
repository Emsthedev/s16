// console.log("Hello");

/*  Arithmetic Operators */

let x = 45;
let y = 28;

console.log("Console Log")
console.log(x + y);
console.log(x - y);
console.log(x * y);
console.log(x / y);

console.log("Discussion")

let sum = x + y;
console.log("Addition: " + sum);

let difference = x - y;
console.log("subtraction: " + difference);

let product = x * y;
console.log("Multiplication: " + product);

let quotient = x / y;
console.log("Quotient: " + quotient);

let modulo = x % y;
console.log("Modulo: " + modulo);

console.log("");
console.log("Assignment Operator")

let assignmentNumber = 8;
console.log(assignmentNumber);


console.log("");
console.log("Arithmetic Assignment Operator")

console.log("Addition Assinment Operator");

assignmentNumber = assignmentNumber +1;
console.log("Result of Addition Assignment Operator: " +assignmentNumber);

assignmentNumber += 1;
console.log( "Shothand += : " + assignmentNumber)

assignmentNumber -= 2;
console.log( "Shothand -= : " + assignmentNumber)

// *= /=  %=

// Parentheis Exponent Multiplication Division Addition Substration
// PEMDAS Order of precedence

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("mdas: "+mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log("result of pemdas: "+pemdas);


// increment and decrement
/* Operators that add or subtract values by 1 and reassigns the value of the variable
where the increment/decrement was applied to */
let z = 1;
console.log("Value of z: " + z);
let increment = ++z; // z=z + 1
console.log("Precrement Value of z: " + increment); // increment+1 z is now 2
/* the value of "z " is added by a value of one before */
console.log("Value of z: " + z);

// post increment
increment = z++;
console.log("Post increment: "+increment);

/* the value of "z" is stored in the value of increment then the 
value of "z" is increased by one */

console.log("Post value of z: "+z);
/* The value of "z was increased again reassigning the value to 3" */

let decrement = --z;
console.log("pre-decrement of z: "+z);
/* the value of "z" is decresed by a value before
storing it inthe variable decrementted Result 2
The value of z is at 3 before it was decremented */


// post decrement 
decrement = z--;
console.log("Post-decrement of z: "+ decrement);
console.log("Value of z: "+ z);


console.log("")


// type of coercion
/* 
Type coercion is the automatic or implicit
conversion of values from one data type to another
- this happens when operations are performed on different data types that would
normally not possible and yielded irregular results
-values are automatically converted from one data type
to another in order to resolve operations



*/
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log("coercion: " +coercion) //1012
console.log(typeof coercion) //string

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log("non coercion: " +nonCoercion);

let numE = true + 1;
console.log("Num E:" +numE);

let numF = false + 2;
console.log("Num F:" +numF);


console.log("")
// Equality Operator

/* 
checkes whether */

console.log(1==1); //true
console.log(1==2); //false
console.log(1=='1'); //true
console.log('Zuitt' == 'Zuitt'); // true
console.log('Zuitt' == 'zuitt'); // false
console.log('A' == 'a'); // 61 == 41 


console.log("")
// strict equality Operator

console.log(1===1); //true
console.log(1==='1'); //false
console.log(false ===0); //false
console.log(false ==0); //true

console.log("")
// Inequality Operator

// checks whether the operands is not equal
// attempts to convert and compare operands that are differenet data types
console.log( 1 != 1); //false
console.log( 1 != 3); //true
console.log(1 != '1'); //false converts



console.log("")
//Strict  Inequality Operator
//looks also if data types are not the same
console.log(1 !== 1); //false
console.log(1 !== '1'); //true




console.log("")
// Relational Operator
/* Some comparison operators
checks one value is greather than / less than to the other value

Like in equality comparison operators, whe
 */

let a = 50;
let b = 65;

console.log(b > a ); //true

let isGreaterThan = a> b;
console.log(isGreaterThan)

let isLessThan = a< b;
console.log(isLessThan)

//GTE greater than or equal (>=)
//LTE (<=)

let GTE = a>b;
console.log(GTE)



console.log("")
// Logical Operator

/* 
AND Operator (&&)
- returns true if all operands are true


p        q    p&&q
F        F     false
f        t     false
t        f     false
t        t      true



OR operator (|| - double pipe)
- retruns true if one of the operands are true

p        q    p||q
F        F     false
f        t     true
t        f     true
t        t      true





*/

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;


let authorization1 = isAdmin && isRegistered;
console.log(authorization1) // f and t = false


let voteAuthorization = isLegalAge && isRegistered;
console.log(voteAuthorization); //t and t = true

let adminAccess = isAdmin && isLegalAge && isRegistered;
console.log(adminAccess); //f and true and true = false

let random = isAdmin && false;
console.log(random); //false


let requiredLevel= 95;
let requiredAge = 18;

let gameTopPlayer = isRegistered && requiredLevel ===25;
console.log(gameTopPlayer); //t and false = false

let gamePlayer = isRegistered && isLegalAge && requiredLevel >=25;

console.log(gamePlayer); //true and true and true


console.log("");

let userName= 'emsthegamer';
let userNameB= 'gamer01';
let userAge= 15;
let userAgeB = 26;

let registration1 = userName.length > 8 && userAge >= requiredAge;
console.log(registration1); // false


let registration2 = userNameB.length > 8 && userAgeB >= requiredAge;
console.log(registration2); // false



console.log("OR opertor")

let userLevel = 100;
let userLevel2 = 65;


let guildRequirement = isRegistered || userLevel >= 
requiredLevel || userAge >= requiredAge;

console.log(guildRequirement); //ture


let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); //false

let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin2); //true ! converts or switch value of is Admin

 console.log(!isRegistered);// false



 console.log("Activity ")

/*  Activity:
1. In the S15 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure 
that the script file is properly associated with the html file.
3. Prompt the user for 2 numbers and perform different arithmetic operations 
based on the total of the two numbers:
  - If the total of the two numbers is less than 10, add the numbers
  - If the total of the two numbers is 10 - 20, subtract the numbers
  - If the total of the two numbers is 11 - 21 multiply the numbers
  - If the total of the two numbers is greater than or equal to 30, divide the numbers
4. Use an alert for the total of 10 or greater and a console warning for the 
total of 9 or less.
5. Prompt the user for their name and age and print out different alert messages
 based on the user input:
  -  If the name OR age is blank/null, print the message are you a time traveler?
  -  If the name AND age is not blank, print the message with the user’s name and age.
6. Create a function named isLegalAge which will check if the user's input 
from the previous prompt is of legal age:
  - 18 or greater, print an alert message saying You are of legal age.
  - 17 or less, print an alert message saying You are not allowed here.
8. Create a switch case statement that will check if the user's age input is 
within a certain set of expected input:
  - 18 - print the message You are now allowed to party.
  - 21 - print the message You are now part of the adult society.
  - 65 - print the message We thank you for your contribution to society.
  - Any other value - print the message Are you sure you're not an alien?
8. Create a try catch finally statement to force an error, print the error message 
as a warning and use the function isLegalAge to print alert another message.
9. Create a git repository named S15.
10. Initialize a local git repository, add the remote link and push to git 
with the commit message of Add activity code.
11. Add the link in Boodle. */

console.log("")

console.log("The result of num1 + num2 should be 30")
console.log("Actual Result")
console.log()


console.log("The result of num3 + num4 should be 200")
console.log("Actual Result")
console.log()

console.log("The result of num5 - num6 should be 7")
console.log("Actual Result")
console.log()

console.log("There are 525600 minutes in a year. ")
console.log("132 degrees Celsius when converted to Farenheit is 269.6")
console.log("The remainder of 165 divided by 8 is: 5")
console.log("is num7 divisible by 8")
console.log()


console.log("The remainder of 348 divided by 4 is: 0")
console.log("is num8 diviscible by 4?")
console.log()


